package com.teste.digioapplication.utils

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.functions.Function
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import java.util.concurrent.Callable

class RxSchedulerRule : TestRule {

    private val schedulerInstance = Schedulers.trampoline()

    private val schedulerFunction = Function<Scheduler, Scheduler> { schedulerInstance }

    private val schedulerFunctionLazy = Function<Callable<Scheduler>, Scheduler> { schedulerInstance }

    override fun apply(base: Statement, description: Description): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                RxAndroidPlugins.reset()
                RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerFunctionLazy)

                RxJavaPlugins.reset()
                RxJavaPlugins.setInitIoSchedulerHandler(schedulerFunctionLazy)
                RxJavaPlugins.setInitComputationSchedulerHandler(schedulerFunctionLazy)
                RxJavaPlugins.setInitNewThreadSchedulerHandler(schedulerFunctionLazy)
                RxJavaPlugins.setInitSingleSchedulerHandler(schedulerFunctionLazy)
                RxJavaPlugins.setIoSchedulerHandler(schedulerFunction)
                RxJavaPlugins.setNewThreadSchedulerHandler(schedulerFunction)
                RxJavaPlugins.setComputationSchedulerHandler(schedulerFunction)

                try {
                    base.evaluate()
                } finally {
                    RxAndroidPlugins.reset()
                    RxJavaPlugins.reset()
                }
            }
        }
    }
}