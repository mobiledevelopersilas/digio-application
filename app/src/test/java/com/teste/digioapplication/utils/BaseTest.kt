package com.teste.digioapplication.utils

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.ClassRule
import org.junit.Rule

abstract class BaseTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    companion object {
        @ClassRule
        @JvmField
        val rxSchedulerRule = RxSchedulerRule()
    }
}