package com.teste.digioapplication.main

import com.teste.digioapplication.data.repository.MainRepository
import com.teste.digioapplication.ui.presenter.MainContract
import com.teste.digioapplication.ui.presenter.MainPresenter
import com.teste.digioapplication.utils.BaseTest
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.mockk.verifyAll
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Test

class MainPresenterTest: BaseTest() {

    private lateinit var mainPresenter: MainContract.Presenter

    private val view = mockk<MainContract.View>(relaxed = true)
    private val repository = mockk<MainRepository>(relaxed = true)
    private val compositeDisposable = mockk<CompositeDisposable>(relaxed = true)

    @Before
    fun setUp() {
        mainPresenter = MainPresenter(view, repository, compositeDisposable)
    }

    @Test
    fun `Get home data returns error`() {
        //Given
        every {
            repository.loadHomeData()
        } returns Single.error(Throwable("Ops, não consegui carregar as informações"))

        //When
        mainPresenter.loadHomeData()

        //Then
        verifyAll {
            view.showLoading()
            view.responseError(any())
            view.hideLoading()
        }

        verify(exactly = 0) {
            view.responseProducts(any())
        }
    }

    @Test
    fun `Get home data returns success`() {
        //Given
        every {
            repository.loadHomeData()
        } returns Single.just(mockk(relaxed = true))

        //When
        mainPresenter.loadHomeData()

        //Then
        verifyAll {
            view.showLoading()
            view.responseProducts(any())
            view.hideLoading()
        }

        verify(exactly = 0) {
            view.responseError(any())
        }
    }

    @Test
    fun `Checking if view is destroyed`() {
        mainPresenter.destroy()
        mainPresenter.loadHomeData()

        verify {
            compositeDisposable.dispose()
        }

        verify(exactly = 0) {
            view.showLoading()
            view.responseProducts(any())
            view.responseError(any())
            view.hideLoading()
        }
    }
}