package com.teste.digioapplication.util

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.google.android.material.snackbar.Snackbar
import com.teste.digioapplication.R



fun Context.getCardProgressDrawable(colorId: Int = R.color.colorAccent,
                                    progressStrokeWidth: Float = 8f,
                                    progressCenterRadius: Float = 32f): Drawable {
    return CircularProgressDrawable(this).apply {
        strokeWidth = progressStrokeWidth
        centerRadius = progressCenterRadius
        setColorSchemeColors(ContextCompat.getColor(this@getCardProgressDrawable, colorId))
        start()
    }
}