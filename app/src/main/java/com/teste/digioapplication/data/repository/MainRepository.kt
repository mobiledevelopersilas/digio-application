package com.teste.digioapplication.data.repository

import com.teste.digioapplication.data.service.MainService

class MainRepository(private val mainService: MainService) {

    fun loadHomeData() = mainService.loadProducts()
}