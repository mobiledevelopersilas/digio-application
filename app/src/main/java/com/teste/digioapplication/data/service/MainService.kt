package com.teste.digioapplication.data.service

import com.teste.digioapplication.model.DigioProducts
import io.reactivex.Single
import retrofit2.http.GET

interface MainService {

    companion object {
        private const val PRODUCTS = "products"
    }

    @GET(PRODUCTS)
    fun loadProducts(): Single<DigioProducts>
}