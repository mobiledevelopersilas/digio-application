package com.teste.digioapplication.data.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.BuildConfig
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RestConfiguration {

    private const val TIME_OUT = 20L

    internal inline fun <reified S> createNewService(url: String): S {
        return Retrofit
            .Builder()
            .baseUrl(url)
            .setupFactories()
            .setupClient()
            .build()
            .create(S::class.java)
    }

    private fun Retrofit.Builder.setupClient(): Retrofit.Builder {
        val okHttpClient = OkHttpClient.Builder()
            .setupTimeoutCalls()
            .setupInterceptors()
            .build()
        return client(okHttpClient)
    }

    private fun Retrofit.Builder.setupFactories(): Retrofit.Builder {
        val converterFactory = GsonConverterFactory.create()
        val rxAdapterFactory = RxJava2CallAdapterFactory.create()

        return addConverterFactory(converterFactory)
            .addCallAdapterFactory(rxAdapterFactory)
    }

    private fun OkHttpClient.Builder.setupTimeoutCalls(): OkHttpClient.Builder {
        return readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            .callTimeout(TIME_OUT, TimeUnit.SECONDS)
    }

    private fun OkHttpClient.Builder.setupInterceptors(): OkHttpClient.Builder {
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            addInterceptor(logging)
        }
        return this
    }


}