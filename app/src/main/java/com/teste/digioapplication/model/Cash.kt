package com.teste.digioapplication.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Cash(
    val title: String,
    val bannerURL: String,
    val description: String
): Parcelable