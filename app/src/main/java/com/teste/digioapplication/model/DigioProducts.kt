package com.teste.digioapplication.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DigioProducts(
    @SerializedName("spotlight")
    val spotlights: ArrayList<Spotlight>,
    val products: ArrayList<Product>,
    val cash: Cash
): Parcelable