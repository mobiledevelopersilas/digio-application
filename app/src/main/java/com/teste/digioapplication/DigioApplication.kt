package com.teste.digioapplication

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.teste.digioapplication.di.ProductsFactory
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class DigioApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@DigioApplication)
            modules(ProductsFactory.module)
        }
    }
}