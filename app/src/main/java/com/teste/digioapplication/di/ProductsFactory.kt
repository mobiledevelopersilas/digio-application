package com.teste.digioapplication.di

import com.teste.digioapplication.BuildConfig
import com.teste.digioapplication.data.api.RestConfiguration
import com.teste.digioapplication.data.repository.MainRepository
import com.teste.digioapplication.data.service.MainService
import com.teste.digioapplication.ui.presenter.MainContract
import com.teste.digioapplication.ui.presenter.MainPresenter
import io.reactivex.disposables.CompositeDisposable
import org.koin.core.qualifier.named
import org.koin.dsl.module

object ProductsFactory {

    const val PRODUCTS_SCOPE = "productsScope"

    val module = module {
        scope(named(PRODUCTS_SCOPE)) {
            scoped<MainService> { RestConfiguration.createNewService(BuildConfig.BASE_URL) }
            scoped { MainRepository(get()) }
            scoped<MainContract.Presenter> { (view: MainContract.View) ->
                MainPresenter(view, get(), CompositeDisposable())
            }
        }
    }
}