package com.teste.digioapplication.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.teste.digioapplication.R
import com.teste.digioapplication.model.Spotlight
import com.teste.digioapplication.util.loadImageWithUrl
import kotlinx.android.synthetic.main.card_spotlight.view.image_view_spotlight

class SpotlightAdapter(
    private val spotlights: ArrayList<Spotlight>,
    private val click: (spotlight: Spotlight) -> Unit,
    private val loadSuccess: () -> Unit,
    private val loadError: (message: String) -> Unit
): RecyclerView.Adapter<SpotlightAdapter.SpotlightViewHolder>() {

    override fun getItemCount() = spotlights.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpotlightViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.card_spotlight, parent, false)
        return SpotlightViewHolder(view)
    }

    override fun onBindViewHolder(holder: SpotlightViewHolder, position: Int) {
        spotlights[position].let { spotlight ->
            holder.itemView.apply {

                image_view_spotlight.loadImageWithUrl(spotlight.bannerURL, loadFinish = {
                    if (position == 0) loadSuccess.invoke()
                }, loadError = {
                    val message =
                        context.getString(R.string.main_load_image_error, spotlight.name)
                    loadError.invoke(message)
                })

                setOnClickListener(holder)
            }
        }
    }

    inner class SpotlightViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        override fun onClick(view: View?) {
            view?.apply {
                click.invoke(spotlights[adapterPosition])
            }
        }
    }

}