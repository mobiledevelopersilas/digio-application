package com.teste.digioapplication.ui.presenter

import androidx.annotation.StringRes
import com.teste.digioapplication.model.DigioProducts

interface MainContract {

    interface View {
        fun showLoading()
        fun hideLoading()
        fun responseProducts(digioProducts: DigioProducts)
        fun responseError(@StringRes errorId: Int)
    }

    interface Presenter {
        fun loadHomeData()
        fun destroy()
    }
}