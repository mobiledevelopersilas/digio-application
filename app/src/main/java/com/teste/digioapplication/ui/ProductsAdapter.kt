package com.teste.digioapplication.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.teste.digioapplication.R
import com.teste.digioapplication.model.Product
import com.teste.digioapplication.util.loadImageWithUrl
import kotlinx.android.synthetic.main.card_product.view.image_view_product

class ProductsAdapter(
    private val products: ArrayList<Product>,
    private val click: (product: Product) -> Unit,
    private val loadSuccess: () -> Unit,
    private val loadError: (message: String) -> Unit
): RecyclerView.Adapter<ProductsAdapter.SpotlightViewHolder>() {

    override fun getItemCount() = products.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpotlightViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.card_product, parent, false)
        return SpotlightViewHolder(view)
    }

    override fun onBindViewHolder(holder: SpotlightViewHolder, position: Int) {
        products[position].let { product ->
            holder.itemView.apply {

                image_view_product.loadImageWithUrl(product.imageURL, loadFinish = {
                    if (position == 0) loadSuccess.invoke()
                }, loadError = {
                    val message = context.getString(R.string.main_load_image_error, product.name)
                    loadError.invoke(message)
                })

                setOnClickListener(holder)
            }
        }
    }

    inner class SpotlightViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        override fun onClick(view: View?) {
            view?.apply {
                click.invoke(products[adapterPosition])
            }
        }
    }

}