package com.teste.digioapplication.ui.presenter

import com.teste.digioapplication.R
import com.teste.digioapplication.data.repository.MainRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainPresenter(
    private var view: MainContract.View? = null,
    private val mainRepository: MainRepository,
    private val compositeDisposable: CompositeDisposable
): MainContract.Presenter {

    override fun loadHomeData() {
        compositeDisposable.addAll(
            mainRepository
                .loadHomeData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.showLoading() }
                .doFinally { view?.hideLoading() }
                .subscribe( { response ->
                    response?.apply {
                        view?.responseProducts(this)
                    } ?: view?.responseError(R.string.main_load_content_error)
                }, { error ->
                    error.printStackTrace()
                    view?.responseError(R.string.main_load_content_error)
                })
        )
    }

    override fun destroy() {
        compositeDisposable.dispose()
        view = null
    }
}