package com.teste.digioapplication.ui

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.*
import com.teste.digioapplication.R
import com.teste.digioapplication.di.ProductsFactory.PRODUCTS_SCOPE
import com.teste.digioapplication.model.DigioProducts
import com.teste.digioapplication.ui.presenter.MainContract
import com.teste.digioapplication.util.custom.CustomLinearSnapHelper
import com.teste.digioapplication.util.hideProgress
import com.teste.digioapplication.util.loadImageWithUrl
import com.teste.digioapplication.util.showProgress
import com.teste.digioapplication.util.showSnackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.cash_container.*
import kotlinx.android.synthetic.main.cash_shimmer_container.*
import kotlinx.android.synthetic.main.products_container.*
import kotlinx.android.synthetic.main.spotlights_container.*
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named


class MainActivity :
    AppCompatActivity(),
    MainContract.View {

    companion object {
        private const val PRODUCTS_SCOPE_ID = "productsScopeId"
    }

    private val productsScope = getKoin().getOrCreateScope(PRODUCTS_SCOPE_ID, named(PRODUCTS_SCOPE))
    private val productsPresenter by productsScope.inject<MainContract.Presenter> {
        parametersOf(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        productsPresenter.loadHomeData()
    }

    override fun onDestroy() {
        productsPresenter.destroy()
        productsScope.close()
        super.onDestroy()
    }

    override fun showLoading() = showProgress()

    override fun hideLoading() = hideProgress()

    override fun responseProducts(digioProducts: DigioProducts) {
        showAllShimmer()
        setupLoadCash(digioProducts)
        setupRecyclerViews(digioProducts)
    }

    override fun responseError(@StringRes errorId: Int) = showSnackbar(errorId)

    private fun showAllShimmer() {
        main_cash_shimmer_container.isVisible = true

        recycler_view_spotlights.showShimmerAdapter()
        shimmer_cash.startShimmerAnimation()
        recycler_view_products.showShimmerAdapter()

        main_cash_container.isVisible = false
        text_view_products_title.isVisible = false
    }

    private fun hideSpotlightShimmer() = recycler_view_spotlights.hideShimmerAdapter()

    private fun hideCashShimmer() {
        shimmer_cash.stopShimmerAnimation()
        main_cash_shimmer_container.isVisible = false
        main_cash_container.isVisible = true
    }

    private fun hideProductsShimmer() {
        recycler_view_products.hideShimmerAdapter()
        text_view_products_title.isVisible = true
    }

    private fun setupLoadCash(digioProducts: DigioProducts) {
        image_view_cash.apply {

            setOnClickListener {
                showSnackbar("Cash")
            }

            loadImageWithUrl(digioProducts.cash.bannerURL, loadFinish = {
                hideCashShimmer()
            }, loadError = {
                val message = getString(R.string.main_load_image_error, digioProducts.cash.title)
                showSnackbar(message)
                hideCashShimmer()
            })
        }
    }

    private fun setupRecyclerViews(digioProducts: DigioProducts){
        recycler_view_spotlights.apply {

            layoutManager = LinearLayoutManager(
                this@MainActivity,
                RecyclerView.HORIZONTAL,
                false
            )

            adapter = SpotlightAdapter(digioProducts.spotlights,
                click = { spotlight ->
                    showSnackbar("Spotlight ${spotlight.name}")
                }, loadSuccess = {
                    hideSpotlightShimmer()
                }, loadError = { message ->
                    showSnackbar(message)
                })

            PagerSnapHelper().attachToRecyclerView(this)
        }

        recycler_view_products.apply {

            layoutManager = LinearLayoutManager(
                this@MainActivity,
                RecyclerView.HORIZONTAL,
                false
            )

            adapter = ProductsAdapter(digioProducts.products,
                click = { product ->
                    showSnackbar("Produto ${product.name}")
                }, loadSuccess = {
                    hideProductsShimmer()
                }, loadError = { message ->
                    showSnackbar(message)
                })
            CustomLinearSnapHelper().attachToRecyclerView(this)
        }
    }
}